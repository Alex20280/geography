package layouts.sourceit.com.geography;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import static layouts.sourceit.com.geography.DbCreator.Country.TABLE_NAME;

public class DbCreator extends SQLiteOpenHelper{
    public static final String DB_NAME = "db_county";
    public static final int DB_VERSION = 1;

    public static class Country implements BaseColumns{
        public static final String TABLE_NAME = "t_name";
        public static final String COUNTRY_NAME = "country_name";
        public static final String COUNTRY_CAPITAL = "country_capital";
    }

    static String SCRIPT_CREATE_TABLE_COUNTRY = "CREATE TABLE " + TABLE_NAME +
            " (" +
            Country._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Country.COUNTRY_NAME + " Text, " +
            Country.COUNTRY_CAPITAL + " INTEGER" +
            ");";

    public DbCreator(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SCRIPT_CREATE_TABLE_COUNTRY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " +TABLE_NAME);
        onCreate(db);
    }
}
