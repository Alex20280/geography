package layouts.sourceit.com.geography;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import layouts.sourceit.com.geography.model.Country;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static layouts.sourceit.com.geography.DbCreator.Country.COUNTRY_CAPITAL;
import static layouts.sourceit.com.geography.DbCreator.Country.COUNTRY_NAME;

public class ProgramActivity extends AppCompatActivity {

    TextView questTextview;
    TextView countyTxview;
    EditText answerField;
    Button replyBtn, startBtn;
    SharedPreferences sharedPreferences;
    int counter = 0;
    int fails = 0;
    DbMaster dbMaster;
    private SQLiteDatabase db;

    private Cursor c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program);

        questTextview = (TextView) findViewById(R.id.questTextview);
        countyTxview = (TextView) findViewById(R.id.countyTxview);
        answerField = (EditText) findViewById(R.id.answerField);
        replyBtn = (Button) findViewById(R.id.replyBtn);
        startBtn = (Button) findViewById(R.id.startBtn);

        Intent programIntent = getIntent();

        SharedPreferences preferences = getSharedPreferences("MYPREFS", MODE_PRIVATE);
        String display = preferences.getString("display", "");
        //textView2.setText(display);

        openDataBase();

        dbMaster = DbMaster.getInstance(this);

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c = db.rawQuery(" SELECT " + " FROM " + COUNTRY_NAME, null);
                c.moveToFirst();
                showCountry();
            }
        });

        replyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reply = answerField.getText().toString().trim();
                if (reply.equals(dbMaster.getCountry())) {
                    counter++;
                    if (counter > 7)
                        Toast.makeText(ProgramActivity.this, "Your knowledge is good!", Toast.LENGTH_LONG).show();
                } else {
                    fails++;
                    if (fails > 5)
                        Toast.makeText(ProgramActivity.this, "You need to learn georaphy better", Toast.LENGTH_LONG).show();
                }
                moveNext();
            }
        });

    }

    private void loadText() {
        SharedPreferences preferences = getSharedPreferences("MYPREFS", MODE_PRIVATE);
        String displayLog = preferences.getString("displayLog", "");
        String displayPass = preferences.getString("displayPass", "");
        questTextview.setText(displayLog);
        //textView2.setText(displayPass);
        Toast.makeText(ProgramActivity.this, "Data Loaded", Toast.LENGTH_LONG).show();
    }

    protected void openDataBase() {
        db = openOrCreateDatabase("db_county", Context.MODE_PRIVATE, null);
    }

    protected void moveNext() {
        c = db.rawQuery(" SELECT " + " FROM " + COUNTRY_CAPITAL, null);
        if (!c.isFirst())
            c.moveToNext();

        showCountry();
    }

    protected void showCountry() {
        String countryName = c.getString(0);
        countyTxview.setText(countryName);
    }
}
