package layouts.sourceit.com.geography;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import layouts.sourceit.com.geography.model.Country;

import static layouts.sourceit.com.geography.DbCreator.Country.COUNTRY_CAPITAL;
import static layouts.sourceit.com.geography.DbCreator.Country.COUNTRY_NAME;
import static layouts.sourceit.com.geography.DbCreator.Country.TABLE_NAME;

public class DbMaster {
    private SQLiteDatabase database;
    private DbCreator dbCreator;

    private static DbMaster instance;

    public DbMaster(Context context){
        dbCreator = new DbCreator(context);
        if (database == null || !database.isOpen()){
            database = dbCreator.getWritableDatabase();
        }
    }

    public static DbMaster getInstance (Context context){
        if (instance == null){
            instance = new DbMaster(context);
        }
        return instance;
    }

    public long insertCountry(Country country){
        ContentValues cv = new ContentValues();
        cv.put(COUNTRY_NAME, country.name);
        cv.put(COUNTRY_CAPITAL, country.capital);
        return database.insert(TABLE_NAME, null, cv);
    }

    public List <Country> getCountry(){
        String query = " SELECT " +
                COUNTRY_NAME + ", " + COUNTRY_CAPITAL +
                " FROM " + TABLE_NAME;
        Cursor cursor = database.rawQuery(query, null);

        List <Country> list = new ArrayList<>();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            Country country = new Country();
            country.name = cursor.getString(0);
            country.capital = cursor.getString(0);
            list.add(country);
            cursor.moveToNext();
        }
        cursor.close();
        return list;

    }

}
