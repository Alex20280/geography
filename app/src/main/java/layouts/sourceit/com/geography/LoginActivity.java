package layouts.sourceit.com.geography;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static layouts.sourceit.com.geography.DbCreator.Country.TABLE_NAME;

public class LoginActivity extends AppCompatActivity {

    TextView loginTextview;
    EditText loginField;
    EditText passwordField;
    Button signinButton;

    DbMaster dbMaster;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginTextview = (TextView) findViewById(R.id.loginTextview);
        loginField = (EditText) findViewById(R.id.loginField);
        passwordField = (EditText) findViewById(R.id.passwordField);
        signinButton = (Button) findViewById(R.id.signinButton);

        Intent loginIntent = getIntent();
        Intent loginScreen = getIntent();

        signinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String login = loginField.getText().toString();
                String password = passwordField.getText().toString();

                SharedPreferences preferences = getSharedPreferences("MYPREFS", MODE_PRIVATE);
                String userDetails = preferences.getString(login, "Login or pass incorrect");

                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("display", userDetails);
                editor.apply();

                Intent programIntent = new Intent(LoginActivity.this, ProgramActivity.class);
                startActivity(programIntent);
            }
        });

    }

}
