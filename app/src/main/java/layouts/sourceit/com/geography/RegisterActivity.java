package layouts.sourceit.com.geography;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {

    TextView registerTextview;
    EditText loginRegField;
    EditText passwordRegField;
    Button registerButton;
    EditText countryEdittext;

    boolean invalid = false;
    //public final String MY_PREFERENCES = "MyPrefs";
    //public final String SAVED_LOGIN = "saved login";
    //public final String SAVED_PASS = "saved pass";
    //public final String SAVED_COUNTRY = "saved country";
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        registerTextview = (TextView) findViewById(R.id.registerTextview);
        loginRegField = (EditText) findViewById(R.id.loginRegField);
        passwordRegField = (EditText) findViewById(R.id.passwordRegField);
        registerButton = (Button) findViewById(R.id.registerButton);
        countryEdittext = (EditText) findViewById(R.id.countryEdittext);

        //sharedPreferences = getApplicationContext().getSharedPreferences(MY_PREFERENCES,MODE_PRIVATE);
        //sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        //passSharedPreferences = getSharedPreferences(SAVED_PASS, Context.MODE_PRIVATE);

        Intent registerIntent = getIntent();

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loginRegField.getText().length() == 0) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(), "Enter your login", Toast.LENGTH_LONG).show();
                } else {
                    if (passwordRegField.getText().length() < 8 && !isValidPassword(passwordRegField.getText().toString())) {
                        invalid = true;
                        Toast.makeText(getApplicationContext(), "Enter your 8 digits password ", Toast.LENGTH_LONG).show();
                    } else {
                        if (countryEdittext.getText().length() == 0) {
                            invalid = true;
                            Toast.makeText(getApplicationContext(), "Enter your country ", Toast.LENGTH_LONG).show();
                        } else {
                            saveData(); // save login, pass and country
                            Intent loginScreen = new Intent(RegisterActivity.this, LoginActivity.class);
                            startActivity(loginScreen);
                        }
                    }
                }
            }
        });

    }

    public void saveData() {
        SharedPreferences prefs = getSharedPreferences("MYPREFS", MODE_PRIVATE);
        String log = loginRegField.getText().toString();
        String pass = passwordRegField.getText().toString();

        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(log, pass + "login data");

        editor.apply();

        //SharedPreferences.Editor country = sp.edit();
        //country.putString(SAVED_COUNTRY, countryEdittext.getText().toString());
        //country.apply();
        Toast.makeText(RegisterActivity.this, "Register information saved", Toast.LENGTH_LONG).show();
    }


    public static boolean isValidPassword(String pass) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[A-Za-z])(?=.*\\\\d)(?=.*[$@$!%*#?&])[A-Za-z\\\\d$@$!%*#?&]{8,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(pass);

        return matcher.matches();
    }

}
