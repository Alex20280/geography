package layouts.sourceit.com.geography;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import layouts.sourceit.com.geography.model.Country;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    TextView titleTxview;
    Button signinBtn;
    Button regBtn;
    DbMaster dbMaster;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        titleTxview = (TextView) findViewById(R.id.titleTxview);
        signinBtn = (Button) findViewById(R.id.signinBtn);
        regBtn = (Button) findViewById(R.id.regBtn);

        dbMaster = DbMaster.getInstance(this);

        Retrofit.getCountries(new Callback<List<Country>>() {
            @Override
            public void success(List<Country> countries, Response response) {

                Toast.makeText(MainActivity.this, "ok", Toast.LENGTH_SHORT).show();

                for (int i = 0; i < 10; i++) {
                    dbMaster.insertCountry(new Country());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MainActivity.this, "wtf", Toast.LENGTH_SHORT).show();

            }
        });

        regBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(registerIntent);
            }
        });

        signinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(loginIntent);
            }
        });

    }


}
